import java.util.Scanner;

public class Ex1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("input a: ");
        double a = Double.parseDouble(in.nextLine());
        System.out.print("input b: ");
        double b = Double.parseDouble(in.nextLine());
        System.out.print("input c: ");
        double c = Double.parseDouble(in.nextLine());

        double res1, res2;
        double max = a > b ? a : b, min = a > b ? b : a;

        if (c > a) {
            res1 = c;
            res2 = max;
        } else if (c > b) {
            res1 = c;
            res2 = min;
        } else {
            res1 = max;
            res2 = min;
        }

        System.out.println(res1 * res1 + " " + res2 * res2);
    }
}
