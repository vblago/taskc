import java.util.Scanner;

public class Ex3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = Integer.parseInt(in.nextLine());

        String str = in.nextLine();
        int min = str.length();
        if (n != 1) {
            for (int i = 1; i < n; i++) {
                String strA = in.nextLine();
                int a = strA.length();
                min = (min < a) ? min : a;
                str = (min < a) ? str : strA;
            }
        }
        System.out.println(String.format("Minimal string: %s; his length %d symbol(s)", str, min));
    }
}
