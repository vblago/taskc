import java.util.Arrays;
import java.util.Scanner;

public class Ex5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = Integer.parseInt(in.nextLine());

        String arr[] = new String[n];

        for (int i = 0; i < n; i++) {
            arr[i] = in.nextLine();
        }

        Arrays.sort(arr);

        for (int i = 1; i < n; i++) {
            for (int j = i; j >= 1; j--) {
                if (arr[j - 1].length() > arr[j].length()) {
                    String interStr = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = interStr;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            System.out.println(arr[i]);
        }
    }
}
