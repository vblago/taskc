import java.util.Scanner;

public class Ex6 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        int n = Integer.parseInt(in.nextLine());
        int counter = 0;
        boolean firstIn = true;

        while (str.indexOf(" ", counter) != -1) {
            counter = str.indexOf(" ", counter);
            if (firstIn) {
                counter = 0;
                firstIn = false;
            }
            boolean bool = false;
            if (counter + n + 2 <= str.length()) {
                //как сократить условие следующей строки (потом добавлю обработку ? и !)?
                if ((str.substring(counter + n + 1, counter + n + 2).equals(" ")) || (str.substring(counter + n + 1, counter + n + 2).equals("."))) {
                    if (str.substring(counter + 1, counter + 2).matches("^(?ui:[бвгджзйклмнпрстфхцчшщ]).*")) {
                        for (int i = counter + 2; i < counter + n; i++) {
                            if (str.substring(i, i + 1).matches("^(?ui:[а-я]).*")) {
                                bool = true;
                            } else {
                                bool = false;
                                break;
                            }
                        }
                    }
                }
                if (bool == true) {
                    str = str.substring(0, counter) + str.substring(counter + n + 1, str.length());
                    counter--;
                }
            }
            counter++;
        }
        System.out.println(str);
    }
}
