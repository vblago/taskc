import java.util.Scanner;

public class Ex4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = Integer.parseInt(in.nextLine());

        int arr[] = new int[n], sum = 0;
        String strArr[] = new String[n];

        for (int i = 0; i < n; i++) {
            strArr[i] = in.nextLine();
            arr[i] = strArr[i].length();
            sum += arr[i];
        }

        for (int i = 0; i < n; i++) {
            if (arr[i] < sum / n)
                System.out.println(strArr[i]);
        }
    }
}

