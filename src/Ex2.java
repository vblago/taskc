import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("input a: ");
        double a = Double.parseDouble(in.nextLine());
        System.out.print("input b: ");
        double b = Double.parseDouble(in.nextLine());
        System.out.print("input c: ");
        double c = Double.parseDouble(in.nextLine());

        double D = b * b - 4 * a * c;

        if (D > 0) {
            double x1 = (-b + Math.sqrt(D)) / (2 * a);
            double x2 = (-b - Math.sqrt(D)) / (2 * a);
            System.out.println("Roots are: " + x1 + " " + x2);
        } else if (D == 0) {
            System.out.println("2 similar roots: " + -b / (2 * a));
        } else {
            System.out.println("There are no real roots");
        }
    }
}
